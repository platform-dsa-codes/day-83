import java.util.ArrayDeque;
import java.util.Deque;

class Solution {
    public int countStudents(int[] students, int[] sandwiches) {
        Deque<Integer> studentPreferences = new ArrayDeque<>();
        
        // Initialize deque with student preferences
        for (int student : students) {
            studentPreferences.offerLast(student);
        }
        
        int unableToEat = 0;
        int i = 0;
        
        // Loop through the sandwiches
        while (i < sandwiches.length) {
            int preference = studentPreferences.pollFirst(); // Get the preference of the student at the front
            
            if (sandwiches[i] == preference) {
                i++; // Student takes the sandwich
                unableToEat = 0; // Reset unableToEat counter
            } else {
                studentPreferences.offerLast(preference); // Put the student back at the end of the queue
                unableToEat++;
                if (unableToEat == studentPreferences.size()) {
                    break; // If all remaining students cannot eat, break the loop
                }
            }
        }
        
        return studentPreferences.size(); // Return the count of students who couldn't eat
    }
}
